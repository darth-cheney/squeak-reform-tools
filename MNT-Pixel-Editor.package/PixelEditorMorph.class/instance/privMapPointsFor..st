as yet unclassified
privMapPointsFor: aRect
	"In the mapping dictionary, map each point object within the
	given rectangle to the rectangle itself."
	(aRect origin x) to: (aRect corner x) do: [ :x |
		(aRect origin y) to: (aRect corner y) do: [ :y |
			mapping at: (x@y) put: aRect]]