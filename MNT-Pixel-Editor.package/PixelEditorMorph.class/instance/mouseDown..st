as yet unclassified
mouseDown: anEvent
	| localPoint rect |
	localPoint := anEvent position - self position.
	rect := mapping at: localPoint.
	self image getCanvas
		fillRectangle: rect
		color: Color black.
	self changed.
	
	"Update the cached bits for the true
	sized bitmap"
	self updateTrueBitsFor: rect.
	
	updateBlock ifNotNil: [
		updateBlock value: self ].
	
	