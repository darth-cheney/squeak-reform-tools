as yet unclassified
updateTrueBitsFor: aRect
	"Update the cache dicitonary that maps
	actual non-scaled bitmap display points to booleans
	of whether they are actve or not (ie drawn)"
	| actualPoint |
	actualPoint := rectToTruePoints at: aRect.
	mappedBits at: actualPoint put: true.