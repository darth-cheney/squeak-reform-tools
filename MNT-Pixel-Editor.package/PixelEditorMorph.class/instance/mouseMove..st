as yet unclassified
mouseMove: anEvent
	(anEvent buttons = 4 and: [ self containsPoint: anEvent position])
		ifTrue: [
			"Draw the end point of the move"
			self drawBlackAt: (anEvent position - self position).
			anEvent trail do: [ :trailPoint |
				self drawBlackAt: (trailPoint - self position)].
			self changed.
			
			updateBlock ifNotNil: [
				updateBlock value: self ] ].
	