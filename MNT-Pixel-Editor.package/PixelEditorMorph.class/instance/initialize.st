as yet unclassified
initialize
	super initialize.
	mapping := Dictionary new.
	"By default, initialize everything to 300x300
	with a 10px pixel size"
	self
		width: 300;
		height: 300;
		image: (Form extent: 300@300 depth: 8);
		pixelSize: 10;
		setupRectangles.
		
	"Fill the underlying form all white"
	self privDrawBackground 