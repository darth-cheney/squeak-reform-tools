as yet unclassified
drawBlackAt: aPoint
	"Attempt to load the mapped rect for the given
	mouse event point, and draw a black pixel there.
	Simply ignore any cases where the point has no
	mapping (common when moving mouse on border areas
	etc)"
	| rect |
	rect := mapping at: aPoint ifAbsent: [ ^ self ].
	self image getCanvas
		fillRectangle: rect 
		color: Color black.
		
	"Update the cached bits for the true
	sized bitmap"
	self updateTrueBitsFor: rect.