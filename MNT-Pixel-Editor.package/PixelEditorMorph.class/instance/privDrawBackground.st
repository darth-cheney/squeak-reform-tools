as yet unclassified
privDrawBackground
	"Draw the default background for the image"
	self image getCanvas
		fillRectangle:  self image boundingBox
		color: Color white.
		
	self changed