as yet unclassified
setupRectangles
	"Setup both the array of rectangle objects that
	describe the local coordinates of pixels on the Form,
	and a dictionary mapping points to the rectangles in
	which they appear"
	| numHorizRects numVertRects |
	numHorizRects := self width / self pixelSize.
	numVertRects := self height / self pixelSize.
	mapping := Dictionary new.
	mappedBits := Dictionary new.
	rectToTruePoints := Dictionary new.
	rectangles := OrderedCollection new.
	
	1 to: numVertRects do: [ :y |
		1 to: numHorizRects do: [ :x |
			| xOffset yOffset rect |
			yOffset := (y - 1) * self pixelSize.
			xOffset := (x - 1) * self pixelSize.
			rect := ((x + xOffset)@(y + yOffset)) extent: (self pixelSize)@(self pixelSize).
			rectangles add: rect.
			mappedBits at: (x@y) put: false.
			rectToTruePoints at: rect put: (x@y).
			self privMapPointsFor: rect.]]
	