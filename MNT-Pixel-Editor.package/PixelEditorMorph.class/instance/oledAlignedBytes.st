as yet unclassified
oledAlignedBytes
	"Respond with the current bits as a ByteArray that is correctly
	aligned for the Reform's OLED module"
	| bytes |
	bytes := ByteArray ofSize: 512.
	1 to: 4 do: [ :pageNum |
		1 to: 8 do: [ :bitNum |
			| y |
			y := ((pageNum - 1) * 8) + bitNum.
			1 to: 128 do: [ :rowNum |
				| byte byteIndex point |
				byteIndex := ((pageNum - 1) * 128) +  rowNum.
				byte := bytes at: byteIndex.
				point := rowNum@y.
				(mappedBits at: point) ifTrue: [
					bytes
						at: byteIndex
						put: (byte bitAt: bitNum put: 1) ] ] ] ].
		
	^ bytes