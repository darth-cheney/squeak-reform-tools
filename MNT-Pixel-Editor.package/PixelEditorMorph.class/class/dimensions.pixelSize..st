as yet unclassified
dimensions: aPoint pixelSize: anInteger
	"Respond with an initialized instance that has the
	corresponding dimensions in pixelSize"
	^ self new
		width: aPoint x * anInteger;
		height: aPoint y * anInteger;
		pixelSize: anInteger;
		image: (Form extent: (aPoint x * anInteger)@(aPoint y * anInteger) depth: 8);
		setupRectangles;
		privDrawBackground;
		yourself
		