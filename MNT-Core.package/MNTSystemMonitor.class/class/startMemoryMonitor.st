as yet unclassified
startMemoryMonitor

	"If there is already a memory monitoring
	process, do nothing"
	Process allInstances 
		detect: [ :proc |
			proc name = #MNTMemory ]
		ifFound: [ :proc |
			proc isTerminated ifFalse: [ ^ self ]].
		
	"Otherwise, we create the new process and have it check
	every second"
	[
		[
			| pipe |
			pipe := OSPipe nonBlockingPipe.
			ThisOSProcess thisOSProcess
				redirectStdOutTo: pipe writer;
				waitForCommand: 'free'.
				self handleFreeCommand: pipe upToEnd.
				(Delay forSeconds: 1) wait.
		] repeat
	] forkNamed: #MNTMemory