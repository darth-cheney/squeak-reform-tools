as yet unclassified
handleFreeCommand: aString
	"Handle the string returned from
	running the 'free' unix command"
	| parts free total used percent |
	parts := aString lines second findTokens.
	total := parts second asInteger.
	free := parts third asInteger.
	used := parts fourth asInteger.
	percent := (used / total) asFloat * 100.
	Transcript show: percent asString; cr.