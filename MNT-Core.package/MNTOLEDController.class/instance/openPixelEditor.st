as yet unclassified
openPixelEditor
	| editor |
	editor := PixelEditorMorph dimensions: 128@32 pixelSize: 6.
	editor onUpdate: [ :imageMorph |
		self
			writeBitsAtOffset: 0
			bytes: imageMorph oledAlignedBytes ].
	editor openInWindow.