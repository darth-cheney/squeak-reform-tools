as yet unclassified
bytesFromForm: aForm
	| cacheForm bytes |
	bytes := ByteArray ofSize: 512.
	cacheForm := Form extent: 128@32 depth: 1.
	cacheForm getCanvas
		drawImage: (aForm asFormOfDepth: 1)
		at: 0@0.
		
	"page"
	0 to: 3 do: [ :pageNum |
		1 to: 128 do: [ :x |
			1 to: 8 do: [ :bitIndex |
				| byte byteIndex |
				byteIndex := (pageNum * 128) + x.
				byte := bytes at: byteIndex.
				(cacheForm colorAt: x@((pageNum * 8) + bitIndex)) = Color white
					ifFalse: [ bytes at: byteIndex put: (byte bitAt: bitIndex put: 1) ] ] ] ].
		
	^ bytes
		
	
		
	
		
	