as yet unclassified
writeBitsAtOffset: anInteger bytes: aByteArray
	| buffer offset |
	offset := ByteArray ofSize: 2.
	buffer := WriteStream on: ByteArray new.
	offset unsignedShortAt: 1 put: anInteger bigEndian: true.
	buffer
		nextPutAll: 'xWBIT';
		nextPutAll: offset;
		nextPutAll: aByteArray.
		
	self location asFile writeStreamDo: [:s | s nextPutAll: buffer contents ]