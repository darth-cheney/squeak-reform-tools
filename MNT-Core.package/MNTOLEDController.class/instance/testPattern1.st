as yet unclassified
testPattern1
	| stream |
	stream := WriteStream on: ByteArray new.
	1 to: 512 do: [ :idx | stream nextPut: 81 ].
	self
		writeBitsAtOffset: 2 bytes: stream contents 
		